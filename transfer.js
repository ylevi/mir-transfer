const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const {FireblocksSDK, PeerType, TransactionOperation, TransactionStatus} = require("fireblocks-sdk");
const {LCDClient, MsgExecuteContract, ModeInfo, SignerInfo, SimplePublicKey, SignDoc, Fee} =
    require('@terra-money/terra.js');

// ===================
// Client Configuration
// ===================
// The Vault ID where the MIR tokens correlated address is positioned at.
const vaultId = ""
// The address can be found under the vault, under "LUNC".
const sourceAcc = "terra1jucupy5asr7y345jnvcdueajjce6qjuzz60w3v"
const destAcc = "terra14wd9mktucngvca48hgnggaxtdzee34e5fdjktn"
// 1 MIR Token is considered 1000000 (1 million). To transfer 250 MIR Tokens - transferAmount = 250000000
const transferAmount = ""
const apiSecret = fs.readFileSync(path.resolve(".", "./secret.key"), "utf8");
const apiKey = "";
// ===================
// End of Client Configuration
// ===================

const fireblocks = new FireblocksSDK(apiSecret, apiKey);
const chain = "https://lcd.terra.dev"
const chainID = "columbus-5"
const terraClient = new LCDClient({
    URL: chain,
    chainID: chainID
})


async function queryBalance(address) {
    return await terraClient.bank.balance(address)
}

async function queryMir(address, contract) {
    return await terraClient.wasm.contractQuery(contract, {
        "balance": {
            "address": address
        }
    })
}

async function mirTransferMsg(amount, sender, contract, dest) {
    return new MsgExecuteContract(
        sender,
        contract,
        {
            "transfer": {
                "amount": amount,
                "recipient": dest
            }
        }
    )
}


async function mirTransferTranscation(signer, msg) {
    const accountInfo = await terraClient.auth.accountInfo(sourceAcc)
    const seq = accountInfo.getSequenceNumber()
    const txOpt = {
        msgs: [msg]
    }
    const signers = [
        {
            address: signer
        }
    ]
    const estimatedFeeObj = terraClient.tx.estimateFee([
        {
            sequenceNumber: seq
        }
    ], txOpt)
    const gasLimit = (await estimatedFeeObj).gas_limit
    const gasFee = Math.ceil(gasLimit * 0.15 * 62)
    const gasFeeStr = gasFee.toString() + "uluna"

    return await terraClient.tx.create([
            {
                address: signer
            }
        ],
        {
            msgs: [msg],
            feeDenoms: [
                "uluna"
            ],
            fee: new Fee(gasLimit, gasFeeStr)
        }
    )

}

async function createSignedTransaction(vault_id, transaction) {
    const accountInfo = await terraClient.auth.accountInfo(sourceAcc)
    const seq = accountInfo.getSequenceNumber()
    const vaultArgs = {
        assetId: "LUNA",
        vaultAccountId: vault_id,
        change: 0,
        addressIndex: 0,
        compressed: true
    }
    const pubKey = await fireblocks.getPublicKeyInfoForVaultAccount(vaultArgs)
    const byteKey = Buffer.from(pubKey.publicKey, 'hex')
    const strKey = byteKey.toString('base64')
    const pubKeyObj = new SimplePublicKey(strKey)

    const mode = new ModeInfo.fromData({
        single: {
            mode: 'SIGN_MODE_DIRECT'
        }
    })
    const signerInfo = new SignerInfo(
        pubKeyObj,
        seq,
        mode
    )

    transaction.auth_info.signer_infos.push(signerInfo)
    const accountNumber = accountInfo.account_number
    const signedDoc = new SignDoc(
        chainID,
        accountNumber,
        seq,
        transaction.auth_info,
        transaction.body
    )

    const bytesDoc = signedDoc.toBytes(true)
    const content = crypto.createHash("sha256").update(bytesDoc).digest("hex")

    const {status, id} = await fireblocks.createTransaction(
        {
            operation: TransactionOperation.RAW,
            assetId: "LUNA",
            source: {
                type: PeerType.VAULT_ACCOUNT,
                id: vault_id
            },
            note: "SDK Terra JS Transfer - Raw Signing",
            extraParameters: {
                rawMessageData: {
                    messages: [{
                        content
                    }]
                }
            }
        }
    )

    let currentStatus = status
    let txInfo;

    while (currentStatus !== TransactionStatus.COMPLETED && currentStatus !== TransactionStatus.FAILED) {
        try {
            console.log("TX [" + id + "] status: " + currentStatus);
            txInfo = await fireblocks.getTransactionById(id);
            currentStatus = txInfo.status;
        } catch (err) {
            console.log("err", err);
        }
        await new Promise(r => setTimeout(r, 4000));
    }

    if (currentStatus === TransactionStatus.COMPLETED) {
        console.log("TX [" + id + "] status: " + currentStatus);
        const transactionSignature = txInfo.signedMessages[0];
        const fullSig = transactionSignature.signature.fullSig;

        const sigBytes = (Buffer.from(fullSig, 'hex'))
        const sigStr = sigBytes.toString('base64')
        transaction.signatures.push(sigStr)

        return transaction
    } else {
        console.log("Failed completing transaction.")
    }


}


mirTransferMsg(transferAmount, sourceAcc, "terra15gwkyepfc6xgca5t5zefzwy42uts8l2m4g40k6",
    destAcc).then(
    res => {
        mirTransferTranscation(sourceAcc, res).then(result => {
            createSignedTransaction(vaultId, result).then(async tx => {
                console.log("Transaction signed, attempting to broadcast ...")
                const result = await terraClient.tx.broadcast(tx, 180000000)
                console.log("TX Hash is - " + result.txhash)
                console.log("You may find it in the following URL: https://finder.terra.money/classic/tx/" + result.txhash)
            })
        })
    }
)
