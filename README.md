# MIR Transfer

## Disclaimer
Any usage of this script and its outcome is the sole responsibility of the user.
It is best to use this script as a reference and validate any additional requirements on a self written script to
transfer the MIR tokens.

## Getting started
See the below configuration, which is located at the begginning of the file:
```
// ===================
// Client Configuration
// ===================
// The Vault ID where the MIR tokens correlated address is positioned at.
const vaultId = ""
// The address can be found under the vault, under "LUNC".
const sourceAcc = "" 
const destAcc = ""
// 1 MIR Token is considered 1000000 (1 million). To transfer 250 MIR Tokens - transferAmount = 250000000
const transferAmount = ""
const apiSecret = fs.readFileSync(path.resolve(".", "./secret.key"), "utf8");
const apiKey = "";
// ===================
// End of Client Configuration
// ===================
```

* Put the private key of the API user at the same directory as your script.